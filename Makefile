all: pull up

pull:
	docker-compose pull

license-eval.txt:
	curl -o license-eval.txt https://www.juniper.net/us/en/dm/free-vmx-trial/E421992502.txt

id_rsa.pub:
	cp ~/.ssh/id_rsa.pub .

up: license-eval.txt id_rsa.pub
	docker-compose up -d

ps:
	docker-compose ps
	./getpass.sh

down:
	docker-compose down

clean:
	docker system prune -f
