#!/bin/bash

for vmx in bng pe; do
  IP=$(docker-compose logs $vmx |grep 'password to'|cut -d\( -f2|cut -d\) -f1)
  echo "$vmx ($IP)..."
  mv $vmx.conf $vmx.old.txt 2>/dev/null
  ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null $IP "show conf | find ^system | except encrypted-password " > $vmx.conf
  ls -l $vmx.conf
done

